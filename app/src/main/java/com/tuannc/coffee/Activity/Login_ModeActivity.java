package com.tuannc.coffee.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.tuannc.coffee.R;

public class Login_ModeActivity extends AppCompatActivity {
    Button btn1, btn2, btn3;
    ViewDialog viewDialog;
    private FirebaseAuth mAuth;
    private static final String TAG = "AnonymousAuth";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__mode);

        mAuth = FirebaseAuth.getInstance();

        btn1 = findViewById(R.id.anymous);
        btn2 = findViewById(R.id.login1);
        btn3 = findViewById(R.id.registry1);
//        viewDialog = new ViewDialog(this);
        final Login_ModeActivity THIS = this;

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(THIS, AnonymousActivity.class);
                startActivity(intent);

            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(THIS, LoginActivity.class);
                startActivity(intent);


            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(THIS, RegisterActivity.class);
                startActivity(intent);



            }
        });

    }


}
