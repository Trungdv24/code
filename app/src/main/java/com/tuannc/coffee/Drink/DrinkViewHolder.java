package com.tuannc.coffee.Drink;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tuannc.coffee.Model.ItemClickListener;
import com.tuannc.coffee.R;

public class DrinkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView drink_name;
    public ImageView drink_image;

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public DrinkViewHolder(View itemView) {
        super( itemView );

        drink_name = itemView.findViewById( R.id.food_name_fi );
        drink_image = itemView.findViewById( R.id.food_image_fi );

        itemView.setOnClickListener( this );
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v, getAdapterPosition(), false );

    }
}
