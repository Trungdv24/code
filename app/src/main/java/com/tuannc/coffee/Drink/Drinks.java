package com.tuannc.coffee.Drink;



public class Drinks {

    private String Discount;
    private String Name;
    private String Image;
    private String Price;

    public Drinks() {

    }

    public Drinks(String name, String image, String price,String discount) {
        Name = name;
        Image = image;
        Discount = discount;
        Price = price;
    }
    public String getName() {
        return Name;
    }
    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }


}