package com.tuannc.coffee.Adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tuannc.coffee.R;

public class OrderRecyclerViewAdapter extends RecyclerView.ViewHolder {
    ImageView imgOrder;
    TextView txtNameOrder;
    TextView txtPriceOrder;

    public OrderRecyclerViewAdapter(@NonNull View itemView) {
        super(itemView);
        imgOrder = itemView.findViewById(R.id.img_order);
        txtNameOrder = itemView.findViewById(R.id.txt_name_order);
        txtPriceOrder = itemView.findViewById(R.id.txt_price);
    }
}
