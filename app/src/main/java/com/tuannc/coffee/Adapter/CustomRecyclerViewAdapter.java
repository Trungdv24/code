package com.tuannc.coffee.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

//import com.tuannc.coffee.Adapter.OnClickItemUser;
import com.tuannc.coffee.R;

import java.util.ArrayList;

public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.ViewHolder> {
    ArrayList listItem;
    Context context;
    private OnClickItemUser onClickItemUser;

    public CustomRecyclerViewAdapter(ArrayList listItem, Context context, OnClickItemUser onClickItemUser) {
        this.listItem = listItem;
        this.context = context;
        this.onClickItemUser = onClickItemUser;
    }

    @NonNull
    @Override
    public CustomRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_list_user, null);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(params);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomRecyclerViewAdapter.ViewHolder holder, final int position) {
        holder.txtNameUser.setText("User " + position);

        holder.imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "User " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNameUser;
        ImageView imgUser;

        public ViewHolder(View itemView) {
            super(itemView);
            txtNameUser = itemView.findViewById(R.id.txt_name_user);
            imgUser = itemView.findViewById(R.id.img_user);
        }
    }
}
