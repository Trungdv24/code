package com.tuannc.coffee.Model;

public class Food {

    private String Image;
    private String Name;
    private String Description;
    private String Price;
    private String Discount;
    private String MenuID;

    public Food(String name, String image, String description, String price, String discount, String menuID) {
        this.Image = image;
        this.Name = name;
        this.Description = description;
        this.Price = price;
        this.Discount = discount;
        this.MenuID = menuID;
    }

    public Food() {
    }
    public String getImage(){
        return Image;
    }
    public void setImage(String image){
        Image = image;
    }
    public String getName(){
        return Name;
    }
    public void setName(String name){
        Name = name;
    }
    public String getDescription(){
        return Description;
    }
    public void setDescription(String description){
        Description = description;
    }
    public String getPrice(){
        return Price;
    }
    public void setPrice(String price){
        Price = price;
    }
    public String getDiscount(){
        return Discount;
    }
    public void setDiscount(String discount){
        Discount = discount;
    }
    public String getMenuID(){
        return MenuID;
    }
    public void setMenuID(String menuID){
        MenuID = menuID;
    }
}


