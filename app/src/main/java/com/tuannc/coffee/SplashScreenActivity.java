package com.tuannc.coffee;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.tuannc.coffee.Activity.Login_ModeActivity;

import gr.net.maroulis.library.EasySplashScreen;

public class SplashScreenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EasySplashScreen config = new EasySplashScreen(SplashScreenActivity.this)
                .withFullScreen()
                .withTargetActivity(Login_ModeActivity.class)
                .withSplashTimeOut(4000)
                .withBackgroundColor(Color.parseColor("#E8EFF8"))
                .withFooterText("Coffee")
                .withBeforeLogoText("Coffee app")
                .withAfterLogoText("Welcome to vCoffee")
                .withLogo(R.drawable.main_logo);

        config.getFooterTextView().setTextColor(Color.parseColor("#6777DF"));
        config.getBeforeLogoTextView().setTextColor(Color.parseColor("#6777DF"));
        config.getAfterLogoTextView().setTextColor(Color.parseColor("#6777DF"));

        View easySplashScreen = config.create();
        setContentView(easySplashScreen);

    }
}


