package com.tuannc.coffee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.tuannc.coffee.Drink.HomeActivity;
import com.tuannc.coffee.Activity.InfoActivity;
import com.tuannc.coffee.Activity.LoginActivity;
import com.tuannc.coffee.Activity.ViewDialog;
import com.tuannc.coffee.Feedback.FeedbackActivity;
import com.tuannc.coffee.Gift.giftmenuActivity;


public class StartActivity extends Activity {
    LinearLayout btnDrinkMenu, btnChat, btnGift, btnPaybill, btnMyInfo, btnFeedback;
    ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        btnDrinkMenu = findViewById(R.id.btn_drink_menu);
        btnChat = findViewById(R.id.btn_chat);
        btnGift = findViewById(R.id.btn_gift);
        btnPaybill = findViewById(R.id.btn_paybill);
        btnMyInfo = findViewById(R.id.btn_my_info);
        btnFeedback = findViewById(R.id.btn_feedback);


//        viewDialog = new ViewDialog(this);

        btnDrinkMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        btnGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, giftmenuActivity.class);
                startActivity(intent);

            }
        });
        btnPaybill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });
        btnMyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, InfoActivity.class);
                startActivity(intent);

            }
        });
        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartActivity.this, FeedbackActivity.class);
                startActivity(intent);

            }
        });

    }
}
