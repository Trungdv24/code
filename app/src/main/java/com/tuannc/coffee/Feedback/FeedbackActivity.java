package com.tuannc.coffee.Feedback;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tuannc.coffee.R;
import com.tuannc.coffee.StartActivity;

public class FeedbackActivity extends Activity {

   static RatingBar ratingBar;
   static EditText editText;
   Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        button = (Button)findViewById( R.id.done);
        ratingBar = (RatingBar) findViewById(R.id.ratingbar);
        ratingBar.setNumStars(5);

        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(), "Tổng số ngôi sao:"+ratingBar.getNumStars()+" \nSố lượng đánh giá:"+ratingBar.getRating(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent( FeedbackActivity.this, StartActivity.class);
                startActivity(intent);
            }
        } );

    }
}
