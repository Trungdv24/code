package com.tuannc.coffee.Gift;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tuannc.coffee.Drink.HomeActivity;
import com.tuannc.coffee.R;
import com.tuannc.coffee.StartActivity;

public class giftmenuActivity extends AppCompatActivity {

    Button button1,button2,button3,button4,button5,button6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_giftmenu );
        button3 = findViewById( R.id.douong );

        button3.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( giftmenuActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        } );
    }
}
