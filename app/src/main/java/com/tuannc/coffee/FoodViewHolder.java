package com.tuannc.coffee;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tuannc.coffee.Model.ItemClickListener;

public class FoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView food_name;
    public ImageView food_image;
    private ItemClickListener intemClickListener;

    public FoodViewHolder(@NonNull View itemView) {
        super(itemView);
        food_image = (ImageView) itemView.findViewById(R.id.food_image);
        food_name = (TextView) itemView.findViewById(R.id.food_name);
        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        intemClickListener.onClick(view,getAdapterPosition(),false);
    }
}
